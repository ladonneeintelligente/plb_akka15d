from math import sin
for x in range(-4, 5):
    try:
        print('{:.3f}'.format(sin(x)/x), end=" ")
    except ZeroDivisionError:
        print(" erreur div par zero ", x) #