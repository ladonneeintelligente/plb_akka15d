from collections import defaultdict

# regroupe les valeurs de y
s = [('y', 1), ('b', 2), ('y', 3), ('b', 4), ('r', 1)]
d = defaultdict(list)
for k, v in s:
    d[k].append(v)

print(d.items())
# dict_items([('y', [1, 3]), ('r', [1]), ('b', [2, 4])])

# compte les occurrences des lettres
s = 'mississippi'
d = defaultdict(int)
for k in s:
    d[k] += 1
print(d.items())
# dict_items([('i', 4), ('p', 2), ('s', 4), ('m', 1)])