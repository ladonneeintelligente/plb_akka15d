import collections

# description du type :
Point = collections.namedtuple("Point", "x y z")
# on instancie un point :
point = Point(1.2, 2.3, 3.4)
# on l’affiche :
print("point : [{}, {}, {}]".format(point.x, point.y, point.z)) # point : [1.2, 2.3, 3.4]