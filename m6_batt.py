import argparse

# 1. création du parser
parser = argparse.ArgumentParser(description='Process some integers')
# 2. ajout des arguments
parser.add_argument('integers', metavar='N', type=int, nargs='+', help='an integer for the accumulator')
parser.add_argument('--sum', dest='accumulate', action='store_const', const=sum, default=max, help='sum the integers (default : find the max')
# 3. parsing de la ligne de commande
args = parser.parse_args()
# processing
print(args.accumulate(args.integers))