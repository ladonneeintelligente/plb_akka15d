def cube(x):
    """retourne le cube de <x>."""
    return x ** 3


# auto-test ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if __name__ == '__main__':  # vrai car module principal
    if cube(9) == 729:
        print("OK !")
    else:
        print("KO !")



