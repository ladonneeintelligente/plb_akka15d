import doctest

# Module 4 AKKA 15 D - Testing with Python slide Loic Gouarin
# 08/12/20 by xav
# Test with doctest


def addition(a, b):
    """
    renvoie l'addition de 2 nombres
    >>> addition(4, 5)
    9
    >>> addition(100, 0.00005)
    100.00005
    """
    return a + b


if __name__ == "__main__":
    doctest.testmod()