import unittest
from monmodule import *


class TestSimple2(unittest.TestCase):
    def setUp(self) -> None:
        self.a = 4.0
        self.b = 5.0
        # changer b par 0 va lever une exception

    def test01_Division(self):
        self.assertEqual(division(self.a, self.b), self.a / self.b)

    def test02_DivisionByZero(self):
        self.assertRaises(ZeroDivisionError, division, self.a, 0.)

    def test03_Division(self):
        self.assertAlmostEqual(division(1., 3), 0.33333, 5)


if __name__ == '__main__':
    unittest.main()
