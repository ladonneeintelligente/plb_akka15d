""" Module de calculs """


def moyenne(*args) :
    """ Renvoie la moyenne."""
    length = len(args)
    sum = 0
    for arg in args :
        sum += arg
    return sum/length


def division(a, b) :
    """
    Renvoie la division.
    """
    return a/b