import sys
import unittest
from os.path import abspath, dirname
from calcul import division, moyenne

# on enrichie le path pour ajouter le répertoire absolu du source à tester :
sys.path.insert(0, dirname(dirname((abspath(__file__)))))

# définition de classe et de fonction -----------------------------------------
class CalculTest(unittest.TestCase) :
    def test_moyenne(self):
        self.assertEqual(moyenne(1, 2, 3), 2)
        self.assertEqual(moyenne(2, 4, 6), 4)

    def test_division(self):
        self.assertEqual(division(10, 5), 2)
        self.assertRaises(ZeroDivisionError, division, 10, 0)

    def test_suite(self):
        tests = [unittest.makeSuite(CalculTest)]
        return unittest.TestSuite(tests)


# auto-test ===================================================================
if __name__ == '__main__':
    unittest.main()
