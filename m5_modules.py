def ok(message) :
    """	Retourne True si on saisie <Entrée>, <O>, <o>, <Y> ou <y>,False dans tous les autres cas."""
    s = input(message + " (O/n) ? ")
    return True if s == "" or s[0] in "OoYy" \
        else False


# auto-test ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if __name__ == '__main__':
    while True:
        if ok("Encore"):
            print('Je continue')
        else:
            print('Je m\'arrête')
            break
