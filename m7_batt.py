import calendar, datetime, time

moon_apollo11 = datetime.datetime(1969, 7, 20, 20, 17, 40)
print(moon_apollo11)
print(time.asctime(time.gmtime(0)))
# Thu Jan 01 00:00:00 1970 (”epoch” UNIX)
vendredi_precedent = datetime.date.today()
un_jour = datetime.timedelta(days=1)

while vendredi_precedent.weekday() != calendar.FRIDAY :
   vendredi_precedent -= un_jour
   print(vendredi_precedent.strftime('%A, %d-%b-%Y'))
