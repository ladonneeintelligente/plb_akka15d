class AucunChiffre(Exception):
    """
        chaîne de caractères contenant aussi autre chose que des chiffres
        """

    pass


def conversion(s):
    """
    conversion d'une chaîne de caractères en entier
    """
    if not s.isdigit():
        raise AucunChiffre(s)
    return int(s)

try:
    s = "123a"
    print(s, " = ", conversion(s))
except AucunChiffre as exc:
    # on affiche ici le commentaire associé à la classe d'exception
    # et le message associé
    print(AucunChiffre.__doc__, " : ", exc)