def inverse(x):
    if x == 0:
        raise ValueError("la valeur nulle est interdite dans la division")
    y = 1.0 / x
    return y

if __name__ == '__main__':
    try:
        print(inverse(0))  # erreur
    except ValueError as exc:
        print("erreur, message: ", exc)

